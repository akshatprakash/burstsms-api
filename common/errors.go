package common

type AppError struct {
	Error          error
	Code           uint
	Message        string
	HttpStatusCode uint
}
