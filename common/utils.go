package common

import (
	"fmt"
	"github.com/ttacon/libphonenumber"
	"regexp"
)

// TODO read list of allowed country codes from config
func ValidateCountryCode(countryCode string) error {
	if countryCode == "" {
		return fmt.Errorf("country code is required")
	}
	if !(countryCode == "AU" ||
		countryCode == "NZ" ||
		countryCode == "SG" ||
		countryCode == "GB" ||
		countryCode == "US") {
		return fmt.Errorf("country code must be AU for Australia, NZ for New Zealand, SG for Singapore, GB for United Kingdom, US for United States")
	}
	return nil
}

func ValidateEmail(email string) bool {
	re := regexp.MustCompile(`^[a-zA-Z0-9._%+\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,24}$`)
	return re.MatchString(email)
}

func ValidateMobile(mobile string, countryCode string) bool {
	phoneNumber, err := libphonenumber.Parse(mobile, countryCode)
	if err != nil {
		return false
	}
	return libphonenumber.IsValidNumber(phoneNumber)
}
