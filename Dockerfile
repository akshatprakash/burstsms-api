# Based on https://blog.codeship.com/building-minimal-docker-containers-for-go-applications/
FROM scratch
WORKDIR /app
ADD burstsms-api /app
ADD config/.env.default /app/config/.env
CMD ["/app/burstsms-api"]