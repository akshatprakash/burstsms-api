package handlers

import (
	"fmt"
	"log"
	"log/syslog"
	"net/http"
	"os"
	"strings"
)

func handleError(w http.ResponseWriter, inputErr error, message string, statusCode uint) {
	LogError(inputErr)
	http.Error(w, message, int(statusCode))
}

func logRemotely(inputErr error) {
	address := "logs6.papertrailapp.com:47982"
	w, networkErr := syslog.Dial("udp", address, syslog.LOG_EMERG|syslog.LOG_KERN, "BurstSMS")
	if networkErr != nil {
		log.Fatal("failed to dial syslog")
	}

	if inputErr != nil {
		w.Err(inputErr.Error())
	}
}

func LogError(inputErr error) {
	var env string = os.Getenv("ENV")
	if strings.ToLower(env) == "development" {
		fmt.Println(inputErr)
	} else {
		logRemotely(inputErr)
	}
}
