package handlers

import (
	"bitbucket.org/akshatprakash/burstsms-api/common"
	"encoding/json"
	"net/http"
	"reflect"
	"strings"
)

type RequestHandler func(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError)

func (fn RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	result, err := fn(w, r)
	if err != nil {
		handleError(w, err.Error, err.Message, err.HttpStatusCode)
		return
	}

	writeResponse(w, r, result)
}

func writeResponse(w http.ResponseWriter, r *http.Request, result interface{}) {
	if strings.EqualFold(r.Method, "GET") {
		write(w, http.StatusOK, result)
	} else if strings.EqualFold(r.Method, "POST") {
		write(w, http.StatusCreated, result)
	}
}

func write(w http.ResponseWriter, statusCode int, result interface{}) {
	if !reflect.ValueOf(result).IsNil() {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		json.NewEncoder(w).Encode(result)
	}
	w.WriteHeader(statusCode)
}
