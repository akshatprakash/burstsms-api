package main

import (
	"bitbucket.org/akshatprakash/burstsms-api/router"
	"github.com/joho/godotenv"
	"log"
)

// BuildNumber records the continuous integration build number
var BuildNumber = "undefined"

// Version records the DVCS commit ref
var Version = "undefined"

// Bootstrap the env variables, mux and connect to the DB
func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	loadEnv()
	router.NewRouter()
	select {}
}

func loadEnv() {
	Version, BuildNumber = Version, BuildNumber
	log.Printf("Version Number: %v, BuildNumber: %v", Version, BuildNumber)
	//Load .env file (not checked into repo for security)
	err := godotenv.Load("config/.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
