Feature: send sms after shortening url's

  Scenario: send sms of 1x140 length with 1 long url's
#    Given mobile no is 481829013
#    And country code is AU
#    When i send sms:
#      """asdfasdf asdf asdf asdf asdf https://burst.transmitsms.com/api-docs/2/sms/get-sms-sent"""
#    Then i check if sms was sent
#    And i check if sms was delivered
#    Then i receive sms:
#      """asdfasdf asdf asdf asdf asdf http://bit.ly/2gmop1T"""
#
#  Scenario: send sms of 2x140 length with 3 long url's
#    Given mobile no is 481829013
#    Given country code is AU
#    When i send sms:
#      """asdfasdf asdf asdf asdf asdf """
#    Then i check if sms was sent
#    And i check if sms was delivered
#    Then i receive sms:
#      """asdfasdf asdf asdf asdf asdf """
#
#  Scenario: send sms of 3x140 length with no url's in it
#    Given mobile no is 481829013
#    Given country code is AU
#    When i send sms:
#      """asdfasdf asdf asdf asdf asdf """
#    Then i check if sms was sent
#    And i check if sms was delivered
#    Then i receive sms:
#      """asdfasdf asdf asdf asdf asdf """
#
#  Scenario: send sms of 3x140 length with 3 url's in it
#    Given mobile no is 481829013
#    Given country code is AU
#    When i send sms:
#      """asdfasdf asdf asdf asdf asdf """
#    Then i check if sms was sent
#    And i check if sms was delivered
#    Then i receive sms:
#      """asdfasdf asdf asdf asdf asdf """
#
#  Scenario: send sms of 4x140 length
#    Given mobile no is 481829013
#    Given country code is AU
#    When i send sms:
#      """asdfasdf asdf asdf asdf asdf """
#    Then i check if sms was sent
#    And i check if sms failed to deliver
#    And i check if sms is pending delivery
#    And i check if sms was delivered
#    Then i receive sms:
#      """asdfasdf asdf asdf asdf asdf """
