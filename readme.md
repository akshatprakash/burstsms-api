#BurstSMS Bitly to SMS Challenge
##Intro
This challenge is intended to help us understand how you approach a task, for it you are asked to produce a small web application that allows users to send an SMS message via our API. You are expected to use and demonstrate your knowledge of best practice. There is no one correct way to accomplish this task and it will form the basis for a discussion at a further interview.

You will be provided with a BurstSMS account to use for this test that will have enough credit for you to send SMS messages for testing.

Please be sure to provide enough technical instructions so we can setup and run your code.(assume that the language runtime will already be available).

Use whatever technologies you feel comfortable with (bearing in mind the technologies in the job specification)

##Requirements
* Create a web form in which a user can enter a phone number and upto 3 SMS messages worth of text
* On submission of the form search and replace any found http urls with a bitly shortlink via the Bitly API
* Once the message is ready, send it to the given phone number as an sms via the HTTP BurstSMS API
* Please do not use the existing BurstSMS API PHP or .NET client libraries