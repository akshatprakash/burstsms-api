# Thanks to https://gist.github.com/TheHippo/7e4d9ec4b7ed4c0d7a39839e6800cc16

# Pass the build number from
OUT 					 := burstsms-api
PKG 					 := bitbucket.org/akshatprakash/burstsms-api
BUILD_NUMBER 	 := $(CIRCLE_BUILD_NUM)
VERSION 			 := $(shell git describe --always --long --dirty)
PKG_LIST 			 := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES 			 := $(shell find . -name '*.go' | grep -v /vendor/)
LDFLAGS 			 := -ldflags "-X main.BuildNumber=${BUILD_NUMBER} -X main.Version=${VERSION}"
REPOSITORY_URL := 724107474513.dkr.ecr.ap-southeast-2.amazonaws.com

default: run

app: deps
	go build -i -v -o ${OUT}-v${VERSION} ${LDFLAGS} ${PKG}
	ln -sf ${OUT}-v${VERSION} ${OUT}

install:
	curl https://glide.sh/get | sh && \
    glide up

run: app
	./${OUT}-v${VERSION}

test:
	@go test -short -race -v ${PKG_LIST}

vet:
	@go vet ${PKG_LIST}

lint:
	@for file in ${GO_FILES} ;  do \
		golint $$file ; \
	done

deps:
	go get -t -d -v ./...

build: vet lint app

docker:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${OUT} ${LDFLAGS} ${PKG}
	docker build -t ${OUT}:${VERSION} .
	rm -f ${OUT}

dockerrun:
	docker run -d -p 8000:8000 --name ${OUT} --network local ${OUT}:${VERSION}

dockerstop:
	docker rm -f ${OUT}

dockerpush: docker
	docker tag ${OUT}:${VERSION} ${REPOSITORY_URL}/${OUT}:${VERSION}
	docker push ${REPOSITORY_URL}/${OUT}:${VERSION}
	rm -Rf build

clean:
	rm -f ${OUT}
	rm -f ${OUT}-v*
	rm -rf vendor

.PHONY: app run test vet lint deps build install dockerise dockerrun dockerstop clean