package url

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type BitlyAdapter struct {
}

func (*BitlyAdapter) ShortenURL(req *ShortenURLRequest) (*ShortenURLResponse, error) {
	client := &http.Client{}
	var err error
	endpoint := fmt.Sprintf("%s?access_token=%s&longUrl=%s&format=json", req.ApiURL, req.AccessToken, req.LongURL.String())
	var httpReq *http.Request
	httpReq, err = http.NewRequest(http.MethodPost, endpoint, nil)
	if err != nil {
		return nil, err
	}
	httpRes, err := client.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer httpRes.Body.Close()

	resp, err := ioutil.ReadAll(httpRes.Body)
	if err != nil {
		return nil, err
	}
	if httpRes.StatusCode >= 400 {
		return nil, fmt.Errorf("error: %s", string(resp))
	}

	var apiRes = ShortenURLResponse{}
	err = json.Unmarshal(resp, &apiRes)
	if err != nil {
		return nil, err
	}
	if apiRes.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(apiRes.StatusTxt)
	}
	return &apiRes, nil
}
