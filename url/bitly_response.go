package url

type ShortenURLResponse struct {
	StatusCode uint         `json:"status_code"`
	StatusTxt  string       `json:"status_txt"`
	Data       ResponseData `json:"data"`
}

type ResponseData struct {
	GlobalHash string `json:"global_hash"`
	LongURL    string `json:"long_url"`
	ShortURL   string `json:"url"`
	Hash       string `json:"hash"`
	NewHash    uint   `json:"new_hash"`
}
