package url

import (
	"fmt"
	"net/url"
)

type ShortenURLRequest struct {
	ApiURL      *url.URL
	AccessToken string
	LongURL     *url.URL
}

func (r ShortenURLRequest) Validate() error {
	if r.ApiURL == nil {
		return fmt.Errorf("missing shorten api url")
	}
	if r.AccessToken == "" {
		return fmt.Errorf("missing shorten api token")
	}
	if r.LongURL == nil {
		return fmt.Errorf("no url to shorten")
	}
	return nil
}
