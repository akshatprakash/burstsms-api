package url

import (
	"net/http"
	"net/url"
	"testing"
)

func TestShorten(t *testing.T) {
	a := &BitlyAdapter{}
	apiURL, _ := url.Parse("https://api-ssl.bitly.com/v3/shorten")
	longURL, _ := url.Parse("https://www.google.com")
	req := &ShortenURLRequest{
		ApiURL:      apiURL,
		AccessToken: "795c147d8e442f090add933f661edaad79eb92fc",
		LongURL:     longURL,
	}
	resp, err := a.ShortenURL(req)
	if err != nil {
		t.FailNow()
		return
	}
	if resp.StatusCode != http.StatusOK {
		t.FailNow()
		return
	}
}
