package main

import (
	"bitbucket.org/akshatprakash/burstsms-api/app"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/gherkin"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"
)

type SendSMSMock struct {
	Request      app.SendSMSRequest
	Response     app.SentSMSInfo
	HttpStatus   int
	HttpResponse string
}

var testData SendSMSMock

func init() {
	loadEnv()
}

func TestMain(m *testing.M) {
	status := godog.RunWithOptions("godogs", func(s *godog.Suite) {
		FeatureContext(s)
	}, godog.Options{
		Format:    "progress",
		Paths:     []string{"features"},
		Randomize: time.Now().UTC().UnixNano(), // randomize scenario execution order
	})

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func mobileNoIs(mobile string) error {
	testData.Request.Mobile = mobile
	return nil
}

func countryCodeIs(countryCode string) error {
	testData.Request.CountryCode = countryCode
	return nil
}

func iSendSms(message *gherkin.DocString) error {
	testData.Request.Message = message.Content
	var httpClient = &http.Client{}
	reqBody := new(bytes.Buffer)
	json.NewEncoder(reqBody).Encode(testData.Request)
	resp, err := httpClient.Post("http://127.0.0.1:8000/v1/send/sms", "application/json", reqBody)
	if err != nil {
		return err
	}
	testData.HttpStatus = resp.StatusCode
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		testData.HttpResponse = string(body)
	}
	if resp.StatusCode == http.StatusOK {
		err = json.Unmarshal(body, &testData.Response)
		if err != nil {
			return err
		}
	}
	return nil
}

func IsSMSSent() error {
	if testData.HttpStatus != http.StatusOK {
		return fmt.Errorf("send sms failed, http status code %s response %s", testData.HttpStatus, testData.HttpResponse)
	}
	return nil
}

func IsSMSDelivered() error {
	if testData.Response.DeliveryStatus != "delivered" {
		return fmt.Errorf("send sms failed")
	}
	return nil
}

func HasSendSmsFailedToDeliver() error {
	if testData.Response.DeliveryStatus != "failed" {
		return fmt.Errorf("send sms did not fail, it is %s", testData.Response.DeliveryStatus)
	}
	return nil
}

func HasSendSmsFailedToSend() error {
	if testData.HttpStatus != http.StatusOK {
		return fmt.Errorf("send sms did not fail to send, server response http status code is %s", testData.HttpStatus)
	}
	return nil
}

func IsSendSMSPending() error {
	if testData.Response.DeliveryStatus != "pending" {
		return fmt.Errorf("sms is not pending, it's %s", testData.Response.DeliveryStatus)
	}
	return nil
}

func iReceiveSms(message *gherkin.DocString) error {
	if message.Content != testData.Response.Message {
		return fmt.Errorf("expected %s but got %s", message.Content, testData.Response.Message)
	}
	return nil
}

func FeatureContext(s *godog.Suite) {
	s.BeforeScenario(initTestData)
	s.Step(`^mobile no is ([^"]*)$`, mobileNoIs)
	s.Step(`^country code is ([^"]*)$`, countryCodeIs)
	s.Step(`^i send sms:$`, iSendSms)
	s.Step(`^i check if sms was sent$`, IsSMSSent)
	s.Step(`^i check if sms was delivered$`, IsSMSDelivered)
	s.Step(`^i check if sms failed to deliver$`, HasSendSmsFailedToDeliver)
	s.Step(`^i check if sms failed to send`, HasSendSmsFailedToSend)
	s.Step(`^i check if sms is pending delivery$`, IsSendSMSPending)
	s.Step(`^i receive sms:$`, iReceiveSms)
}

// TestEmpty is an empty placeholder to run TestMain
func TestEmpty(*testing.T) {

}

func initTestData(arg1 interface{}) {
	testData = SendSMSMock{
		Request: app.SendSMSRequest{
			MobileNumber: &app.MobileNumber{},
		},
		Response: app.SentSMSInfo{},
	}
}
