package app

import (
	"fmt"
	"github.com/joho/godotenv"
	"testing"
)

func init() {
	err := godotenv.Load("../config/.env")
	if err != nil {
		fmt.Printf("Error loading .env file %v", err)
		panic(err)
	}
}

func TestFormatNumber(t *testing.T) {
	req := &MobileNumber{
		Mobile:      "481829013",
		CountryCode: "AU",
	}
	uc := CreateUseCase()
	resp, err := uc.FormatNumber(req)
	if err != nil {
		fmt.Print(err)
		t.FailNow()
	}
	if resp == nil {
		t.FailNow()
	}
}

func TestShortenURLs(t *testing.T) {
	longURL := "https://www.google.com"
	uc := CreateUseCase()
	shortURL, err := uc.ShortenURL(longURL)
	if err != nil {
		fmt.Printf("error while shortening url %v", err)
		t.FailNow()
	}
	if len(shortURL) >= len(longURL) {
		fmt.Printf("url not shortened")
		t.FailNow()
	}
}

func TestSendSMS(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping send sms test in short mode for saving monies")
	}
	req := &SendSMSRequest{
		Message: "asdf asdf asdf https://www.google.com",
		MobileNumber: &MobileNumber{
			CountryCode: "AU",
			Mobile:      "481829013",
		},
	}

	uc := CreateUseCase()
	resp, err := uc.SendSMS(req)
	if err != nil || resp.DeliveryStatus != "delivered" {
		fmt.Printf("failed to send sms due to %v", err)
		t.FailNow()
	}

}
