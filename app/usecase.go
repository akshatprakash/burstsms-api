package app

import (
	"bitbucket.org/akshatprakash/burstsms-api/common"
	"bitbucket.org/akshatprakash/burstsms-api/sms"
	bitly "bitbucket.org/akshatprakash/burstsms-api/url"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

type UseCase struct {
	SMSHandler
	URLShortener
}

func (uc *UseCase) FormatNumber(r *MobileNumber) (*MobileNumber, *common.AppError) {
	apiURL, err := url.Parse(os.Getenv("BURST_SMS_FORMAT_NUMBER_URL"))
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	req := &sms.FormatNumberRequest{
		BurstSMSRequest: sms.BurstSMSRequest{
			ApiURL:    apiURL,
			ApiKey:    os.Getenv("BURST_SMS_API_KEY"),
			ApiSecret: os.Getenv("BURST_SMS_API_SECRET"),
		},
		Mobile:      r.Mobile,
		CountryCode: r.CountryCode,
	}

	if err := req.Validate(); err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	resp, err := uc.SMSHandler.FormatNumber(req)
	if err != nil || resp == nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	if resp.Error.Code != "SUCCESS" {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}

	return &MobileNumber{
		Mobile: strconv.Itoa(int(resp.International)),
	}, nil
}

func (uc *UseCase) ShortenURL(urlStr string) (string, *common.AppError) {
	longURL, err := url.Parse(urlStr)
	if err != nil {
		return urlStr, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	apiURL, err := url.Parse(os.Getenv("BITLY_API_SHORTEN_URL"))
	if err != nil {
		return urlStr, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	req := &bitly.ShortenURLRequest{
		ApiURL:      apiURL,
		AccessToken: os.Getenv("BITLY_API_GENERIC_ACCESS_TOKEN"),
		LongURL:     longURL,
	}
	if err := req.Validate(); err != nil {
		return urlStr, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	resp, err := uc.URLShortener.ShortenURL(req)
	if err != nil || resp == nil {
		return urlStr, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}

	return resp.Data.ShortURL, nil
}

func (uc *UseCase) ShortenMessage(r *SendSMSRequest) (*SentSMSInfo, *common.AppError) {
	// check if mobile no is valid and supported
	formattedNumberData, appErr := uc.FormatNumber(r.MobileNumber)
	if appErr != nil {
		return nil, appErr
	}

	// extract all words from message
	words := strings.Split(r.Message, " ")
	var shortMessage string
	for _, word := range words {
		longURL, err := url.Parse(word)
		if err != nil {
			// this word is not a url, we continue to next word
			continue
		}
		shortURL, appErr := uc.ShortenURL(longURL.String())
		if err != nil {
			// failed to shorten url return error
			return nil, appErr
		}
		// append words to create shortMessage with url's replaced by short url's
		if shortURL == "" {
			shortMessage += word + " "
		} else {
			shortMessage += shortURL + " "
		}
	}
	return &SentSMSInfo{MobileNumber: formattedNumberData, Message: shortMessage}, nil
}

func (uc *UseCase) SendSMS(r *SendSMSRequest) (*SentSMSInfo, *common.AppError) {
	// check if mobile no is valid and supported
	formattedNumberData, appErr := uc.FormatNumber(r.MobileNumber)
	if appErr != nil {
		return nil, appErr
	}

	// extract all words from message
	words := strings.Split(r.Message, " ")
	var shortMessage string
	for _, word := range words {
		longURL, err := url.Parse(word)
		if err != nil {
			// this word is not a url, we continue to next word
			continue
		}
		shortURL, appErr := uc.ShortenURL(longURL.String())
		if err != nil {
			// failed to shorten url return error
			return nil, appErr
		}
		// append words to create shortMessage with url's replaced by short url's
		if shortURL == "" {
			shortMessage += word + " "
		} else {
			shortMessage += shortURL + " "
		}
	}
	apiURL, err := url.Parse(os.Getenv("BURST_SMS_SEND_SMS_URL"))
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	req := &sms.SendSMSRequest{
		BurstSMSRequest: sms.BurstSMSRequest{
			ApiURL:    apiURL,
			ApiKey:    os.Getenv("BURST_SMS_API_KEY"),
			ApiSecret: os.Getenv("BURST_SMS_API_SECRET"),
		},
		To:          formattedNumberData.Mobile,
		CountryCode: r.CountryCode,
		Message:     shortMessage,
	}
	resp, err := uc.SMSHandler.SendSMS(req)
	if err != nil || resp.Error != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	if resp.Error.Code != "SUCCESS" {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	return &SentSMSInfo{MobileNumber: formattedNumberData, Message: shortMessage, ID: resp.MessageID}, nil
}

func (uc *UseCase) GetDeliveryStatus(r *SentSMSInfo) (*SentSMSInfo, *common.AppError) {
	apiURL, err := url.Parse(os.Getenv("BURST_SMS_DELIVERY_STATUS_URL"))
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	req := &sms.GetSMSSentRequest{
		BurstSMSRequest: sms.BurstSMSRequest{
			ApiURL:    apiURL,
			ApiKey:    os.Getenv("BURST_SMS_API_KEY"),
			ApiSecret: os.Getenv("BURST_SMS_API_SECRET"),
		},
		MessageID: r.ID,
	}
	resp, err := uc.SMSHandler.GetSMSSent(req)
	if err != nil || resp.Error != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	if resp.Error.Code != "SUCCESS" {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	if resp.Recipients == nil || len(resp.Recipients) == 0 {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusInternalServerError,
			Error:          err,
		}
	}
	return &SentSMSInfo{Message: r.Message, ID: resp.MessageID, DeliveryStatus: resp.Recipients[0].DeliveryStatus}, nil
}
