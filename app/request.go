package app

import (
	"fmt"
)

type SendSMSRequest struct {
	*MobileNumber
	Message string `json:"message" schema:"message"`
}

func (r SendSMSRequest) Validate() error {
	if err := r.MobileNumber.Validate(); err != nil {
		return err
	}
	if r.Message == "" {
		return fmt.Errorf("message is required")
	}
	if len(r.Message) > 3*140 {
		return fmt.Errorf("message size must not be more then 3x140")
	}
	return nil
}
