package app

import (
	"bitbucket.org/akshatprakash/burstsms-api/url"
	"bitbucket.org/akshatprakash/burstsms-api/sms"
)

func CreateUseCase() InputPort {
	return &UseCase{
		SMSHandler:   &sms.BurstSMSAdapter{},
		URLShortener: &url.BitlyAdapter{},
	}
}
