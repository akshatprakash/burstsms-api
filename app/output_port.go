package app

import (
	"bitbucket.org/akshatprakash/burstsms-api/sms"
	"bitbucket.org/akshatprakash/burstsms-api/url"
)

type SMSHandler interface {
	FormatNumber(request *sms.FormatNumberRequest) (*sms.FormatNumberResponse, error)
	SendSMS(request *sms.SendSMSRequest) (*sms.SendSMSResponse, error)
	GetSMSSent(request *sms.GetSMSSentRequest) (*sms.GetSMSSentResponse, error)
}

type URLShortener interface {
	ShortenURL(request *url.ShortenURLRequest) (*url.ShortenURLResponse, error)
}
