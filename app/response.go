package app

type SentSMSInfo struct {
	*MobileNumber
	Message        string `json:"message" schema:"message"`
	ID             uint   `json:"id" schema:"message_id"`
	DeliveryStatus string `json:"delivery_status"`
}
