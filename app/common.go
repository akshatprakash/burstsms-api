package app

import (
	"bitbucket.org/akshatprakash/burstsms-api/common"
	"fmt"
)

type MobileNumber struct {
	Mobile      string `json:"mobile" schema:"mobile"`
	CountryCode string `json:"countrycode" schema:"countrycode"`
}

func (r MobileNumber) Validate() error {
	if r.Mobile == "" {
		fmt.Errorf("mobile no is required")
	}
	if common.ValidateMobile(r.Mobile, r.CountryCode) {
		return fmt.Errorf("invalid mobile no %s %s", r.CountryCode, r.Mobile)
	}
	if err := common.ValidateCountryCode(r.CountryCode); err != nil {
		return err
	}
	return nil
}
