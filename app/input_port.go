package app

import "bitbucket.org/akshatprakash/burstsms-api/common"

type InputPort interface {
	FormatNumber(req *MobileNumber) (*MobileNumber, *common.AppError)
	ShortenURL(longURL string) (string, *common.AppError)
	ShortenMessage(req *SendSMSRequest) (*SentSMSInfo, *common.AppError)
	SendSMS(req *SendSMSRequest) (*SentSMSInfo, *common.AppError)
	GetDeliveryStatus(req *SentSMSInfo) (*SentSMSInfo, *common.AppError)
}
