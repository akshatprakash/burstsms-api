package app

import (
	"bitbucket.org/akshatprakash/burstsms-api/common"
	"encoding/json"
	"fmt"
	"net/http"
)

// FormatNumber checks if given mobile no. is valid or not
func FormatNumber(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError) {
	defer r.Body.Close()

	var req *MobileNumber
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(req)
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	if err := req.Validate(); err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	uc := CreateUseCase()
	return uc.FormatNumber(req)
}

// ShortenURLs converts all url's present in the sms text to bit.ly shortened versions
func ShortenURL(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError) {
	defer r.Body.Close()

	var req *SendSMSRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(req)
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	if err := req.Validate(); err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}

	uc := CreateUseCase()
	return uc.ShortenURL(req.Message)
}

// ShortenURLs converts all url's present in the sms text to bit.ly shortened versions
func ShortenMessage(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError) {
	defer r.Body.Close()

	var req *SendSMSRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(req)
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	if err := req.Validate(); err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}

	uc := CreateUseCase()
	return uc.ShortenMessage(req)
}

// SendSMS shortens url's in message and sends it to the specified mobile number
func SendSMS(w http.ResponseWriter, r *http.Request) (interface{}, *common.AppError) {
	defer r.Body.Close()

	var req *SendSMSRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(req)
	if err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}
	if err := req.Validate(); err != nil {
		return nil, &common.AppError{
			HttpStatusCode: http.StatusBadRequest,
			Error:          err,
		}
	}

	uc := CreateUseCase()
	resp, appErr := uc.SendSMS(req)
	if appErr != nil {
		return nil, appErr
	}

	return uc.GetDeliveryStatus(resp)
}

// IndexHandler returns the index (default)
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "BurstSMS API")
}

// HealthCheckHandler responds with 200 for load balancing health checks
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}
