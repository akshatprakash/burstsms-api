package router

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"net/http"
	"os"
	"strings"

	app_handlers "bitbucket.org/akshatprakash/burstsms-api/handlers"
)

//NewRouter starts the mux
func NewRouter() (*http.Server, *mux.Router) {
	rtr := mux.NewRouter()
	rtr.StrictSlash(false)
	var allowedOrigins = strings.Split(os.Getenv("ALLOWED_ORIGINS"), ",")
	var listenURL = os.Getenv("LISTEN_URL")
	addBaseRoutes(rtr)
	addAppRoutes(rtr.PathPrefix("/v1").Subrouter())

	n := negroni.New(app_handlers.NewPanicHandler(), negroni.NewLogger(), negroni.NewStatic(http.Dir("public")))
	n.UseHandler(rtr)

	corsHeaderOptions := handlers.AllowedHeaders([]string{"Access-Control-Max-Age", "Origin", "X-Requested-With", "Content-Type", "Accept", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"})
	corsOriginOptions := handlers.AllowedOrigins(allowedOrigins)
	corsMethodOptions := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	corsCredentialsOptions := handlers.AllowCredentials()
	srv := &http.Server{Addr: listenURL}
	srv.Handler = handlers.CORS(corsHeaderOptions, corsOriginOptions, corsMethodOptions, corsCredentialsOptions)(n)
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			app_handlers.LogError(err)
		}
	}()
	return srv, rtr
}
