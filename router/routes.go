package router

import (
	"bitbucket.org/akshatprakash/burstsms-api/app"
	app_handlers "bitbucket.org/akshatprakash/burstsms-api/handlers"
	"github.com/gorilla/mux"
)

// addRoutes is the application routes
func addAppRoutes(r *mux.Router) {
	r.Handle("/format/number", app_handlers.RequestHandler(app.FormatNumber)).Methods("POST")
	r.Handle("/shorten/url", app_handlers.RequestHandler(app.ShortenURL)).Methods("POST")
	r.Handle("/shorten/sms", app_handlers.RequestHandler(app.ShortenMessage)).Methods("POST")
	r.Handle("/send/sms", app_handlers.RequestHandler(app.SendSMS)).Methods("POST")
}

// addBaseRoutes is the base of all our routes
func addBaseRoutes(r *mux.Router) {
	//Index Path
	r.HandleFunc("/", app.IndexHandler).Methods("GET")
	//Health check
	r.HandleFunc("/health-check", app.HealthCheckHandler).Methods("GET")
}
