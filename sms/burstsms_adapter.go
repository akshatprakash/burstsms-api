package sms

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/schema"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type BurstSMSAdapter struct {
}

// Send is the primary method of sending SMS using burst sms send-sms.json api
// You can elect to pass us the recipient numbers from your database each time you make a call,
// or you can elect to store recipient data in a contact list and submit only the list_id to trigger the send.
// This is best for large databases. To add a list please refer to the add-list call.
// Cost data is returned in the major unit of your account currency, e.g. dollars or pounds
// NOTE: If you do not pass the 'from' parameter the messages will be sent from the shared number pool,
// unless you have a leased number on your account in which case it will be set as the Caller ID
// When you set a alphanumeric for Caller ID, messages cannot be replied to
func (a *BurstSMSAdapter) SendSMS(req *SendSMSRequest) (*SendSMSResponse, error) {
	err := req.Validate()
	if err != nil {
		return nil, err
	}
	client := &http.Client{}
	reqBody, err := req.Encode()
	if err != nil {
		return nil, err
	}
	var httpReq *http.Request
	httpReq, err = http.NewRequest(http.MethodPost, req.ApiURL.String(), reqBody)
	if err != nil {
		return nil, err
	}
	httpReq.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	httpReq.SetBasicAuth(req.ApiKey, req.ApiSecret)

	httpRes, err := client.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer httpRes.Body.Close()

	resp, err := ioutil.ReadAll(httpRes.Body)
	if err != nil {
		return nil, err
	}

	var apiRes = SendSMSResponse{}
	err = json.Unmarshal(resp, &apiRes)
	if err != nil {
		return nil, fmt.Errorf("http status: %d unmarshall : %v response: %v", httpRes.StatusCode, err, string(resp))
	}
	if apiRes.Error != nil && apiRes.Code != "SUCCESS" {
		return nil, fmt.Errorf("error code:%s description:%s", apiRes.Error.Code, apiRes.Description)
	}
	return &apiRes, nil
}

// FormatNumber validates number using burstsms format-number.json api
func (a *BurstSMSAdapter) FormatNumber(r *FormatNumberRequest) (*FormatNumberResponse, error) {
	err := r.Validate()
	if err != nil {
		return nil, err
	}
	client := &http.Client{}
	form := url.Values{}
	enc := schema.NewEncoder()
	err = enc.Encode(r, form)
	if err != nil {
		return nil, err
	}
	var httpReq *http.Request
	httpReq, err = http.NewRequest(http.MethodPost, r.ApiURL.String(), strings.NewReader(form.Encode()))
	if err != nil {
		return nil, err
	}
	httpReq.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	httpReq.SetBasicAuth(r.ApiKey, r.ApiSecret)

	httpRes, err := client.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer httpRes.Body.Close()

	resp, err := ioutil.ReadAll(httpRes.Body)
	if err != nil {
		return nil, err
	}

	var apiRes = FormatNumberResponse{}
	err = json.Unmarshal(resp, &apiRes)
	if err != nil {
		return nil, fmt.Errorf("http status: %d unmarshall : %v response: %v", httpRes.StatusCode, err, string(resp))
	}
	if apiRes.Error != nil && apiRes.Code != "SUCCESS" {
		return nil, fmt.Errorf("error code:%s description:%s", apiRes.Error.Code, apiRes.Description)
	}
	return &apiRes, nil
}

func (a *BurstSMSAdapter) GetSMSSent(req *GetSMSSentRequest) (*GetSMSSentResponse, error) {
	err := req.Validate()
	if err != nil {
		return nil, err
	}
	client := &http.Client{}
	reqBody, err := req.Encode()
	if err != nil {
		return nil, err
	}
	var httpReq *http.Request
	httpReq, err = http.NewRequest(http.MethodPost, req.ApiURL.String(), reqBody)
	if err != nil {
		return nil, err
	}
	httpReq.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	httpReq.SetBasicAuth(req.ApiKey, req.ApiSecret)

	httpRes, err := client.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer httpRes.Body.Close()

	resp, err := ioutil.ReadAll(httpRes.Body)
	if err != nil {
		return nil, err
	}

	var apiRes = GetSMSSentResponse{}
	err = json.Unmarshal(resp, &apiRes)
	if err != nil {
		return nil, fmt.Errorf("http status: %d unmarshall : %v response: %v", httpRes.StatusCode, err, string(resp))
	}
	if apiRes.Error != nil && apiRes.Code != "SUCCESS" {
		return nil, fmt.Errorf("error code:%s description:%s", apiRes.Error.Code, apiRes.Description)
	}
	return &apiRes, nil
}
