package sms

import (
	"bitbucket.org/akshatprakash/burstsms-api/common"
	"fmt"
	"github.com/gorilla/schema"
	"net/url"
	"reflect"
	"strings"
	"time"
)

type BurstSMSRequest struct {
	ApiURL    *url.URL `json:"-"                          schema:"-"`
	ApiKey    string   `json:"-"                          schema:"-"`
	ApiSecret string   `json:"-"                          schema:"-"`
}

func (r BurstSMSRequest) Validate() error {
	if r.ApiURL == nil {
		return fmt.Errorf("ApiURL is mandatory")
	}
	if r.ApiKey == "" {
		return fmt.Errorf("ApiKey is mandatory")
	}
	if r.ApiSecret == "" {
		return fmt.Errorf("ApiSecret is mandatory")
	}
	return nil
}

// SendSMSRequest is the input to Send SMS function of BurstSMS API
// To is the number or set of up to 10,000 numbers to send the SMS to.
//	  If your number set has some invalid numbers, they won’t cause validation error,
//	  but will be returned as ‘fails’ parameter of the response.
//	  Number must be defined in international format.
//	  Some examples by destination:
//	  AU 61491570156, NZ 64212670129, SG 6598654321, UK 44750017696, US 1213811413
// From is the alphanumeric Caller ID, mobile numbers should be in international format. Maximum 11 characters.
//    No spaces. If not set will use shared number pool.
// SendAt is time in the future to send the message Note: All returned timestamps are in ISO_8601 format
//    e.g. YYYY-MM-DD HH:MM:SS. The zone is always UTC.
// ListID is the numerical reference to one of your recipient lists
//    Note: List ID's are made up of digits and will be returned by the add-list call,
//    or can be found at any time by logging into your account and visiting your contacts page.
// DeliveryCallback is the URL on your system which we can call to notify you of Delivery Receipts.
//    If required, this Parameter can be different for each message sent and will take precedence
//    over the DLR Callback URL supplied by you in the API Settings.
// ReplyCallback is a URL on your system which we can call to notify you of incoming messages.
//    If required, this parameter can be different and will take precedence
//    over the Reply Callback URL supplied by you on the API Settings.
// Validity is the maximum time to attempt to deliver. In minutes, 0 (zero) implies no limit.
// RepliesToEmail is the email address to send responses to this message.
//    Note: specified email must be authorised to send messages via add-email or in your account under the 'Email SMS' section.
// FromShared forces sending via the shared number when you have virtual numbers
// CountryCode formats numbers given to international format for this 2 letter country code.
//    i.e. 0422222222 will become 6142222222 when countrycode is AU.
//    Codes available AU Australia, NZ New Zealand SG Singapore GB United Kingdom US United States
type SendSMSRequest struct {
	BurstSMSRequest  `json:"-"                          schema:"-"`
	CountryCode      string   `json:"countrycode,omitempty"      schema:"countrycode,omitempty"`
	To               string   `json:"to,omitempty"               schema:"to,omitempty"`
	Message          string   `json:"message"                    schema:"message"`
	From             string   `json:"from,omitempty"             schema:"from,omitempty"`
	SendAt           *SendAt  `json:"send_at,omitempty"          schema:"send_at,omitempty"`
	ListID           uint     `json:"list_id,omitempty"          schema:"list_id,omitempty"`
	DeliveryCallback *url.URL `json:"dlr_callback,omitempty"     schema:"dlr_callback,omitempty"`
	ReplyCallback    *url.URL `json:"reply_callback,omitempty"   schema:"reply_callback,omitempty"`
	Validity         uint     `json:"validity,omitempty"         schema:"validity,omitempty"`
	RepliesToEmail   string   `json:"replies_to_email,omitempty" schema:"replies_to_email,omitempty"`
	FromShared       string   `json:"from_shared,omitempty"      schema:"from_shared,omitempty"`
}

func (r SendSMSRequest) Validate() error {
	if err := r.BurstSMSRequest.Validate(); err != nil {
		return err
	}
	if r.Message == "" {
		return fmt.Errorf("message is mandatory")
	}
	// TODO Number or set of up to 10,000 numbers to send the SMS to. If your number set has some invalid numbers, they won’t cause validation error, but will be returned as ‘fails’ parameter of the response (see example 3).
	// TODO Number must be defined in international format.
	// Some examples by destination:
	// AU 61491570156, NZ 64212670129, SG 6598654321, UK 44750017696, US 1213811413
	if r.To != "" {

	}
	// TODO validate alphanumeric Caller ID, mobile numbers should be in international format.
	// TODO If not set will use shared number pool.
	if r.From != "" {
		if len(r.From) > 11 {
			return fmt.Errorf("from cannot be longer then 11 chars")
		}
		if strings.Contains(r.From, " ") {
			return fmt.Errorf("from cannot contain spaces")
		}
	}
	if r.SendAt != nil {
		zone, _ := r.SendAt.Zone()
		if zone != time.UTC.String() {
			return fmt.Errorf("send_at time zone must be UTC")
		}
		if r.ListID == 0 && r.To == "" {
			return fmt.Errorf("either of list_id or to must be set")
		}
		if r.SendAt.Before(time.Now().UTC()) {
			return fmt.Errorf("send_at must be in the future")
		}
	}

	if r.RepliesToEmail != "" && !common.ValidateEmail(r.RepliesToEmail) {
		fmt.Printf("invalid replies_to_email %s", r.RepliesToEmail)
	}

	return common.ValidateCountryCode(r.CountryCode)
}

func (r SendSMSRequest) Encode() (*strings.Reader, error) {
	form := url.Values{}
	enc := schema.NewEncoder()
	enc.RegisterEncoder(r.SendAt, func(v reflect.Value) string {
		if v.IsNil() {
			return ""
		} else {
			t := v.Interface().(*SendAt)
			if t.Hour() == 0 && t.Minute() == 0 && t.Second() == 0 && t.Nanosecond() == 0 {
				return t.Format("2006-01-02")
			}
			return t.Format("2006-01-02 15:04:05")
		}
	})
	enc.RegisterEncoder(r.Message, func(v reflect.Value) string {
		if v.IsValid() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.RepliesToEmail, func(v reflect.Value) string {
		if v.IsValid() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.From, func(v reflect.Value) string {
		if v.IsValid() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.ListID, func(v reflect.Value) string {
		if v.Uint() > 0 {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.Validity, func(v reflect.Value) string {
		if v.Uint() > 0 {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.DeliveryCallback, func(v reflect.Value) string {
		if !v.IsNil() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.FromShared, func(v reflect.Value) string {
		if v.IsValid() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.ReplyCallback, func(v reflect.Value) string {
		if !v.IsNil() {
			return v.String()
		}
		return ""
	})
	err := enc.Encode(r, form)
	if err != nil {
		return nil, err
	}
	return strings.NewReader(form.Encode()), nil
}

// FormatNumberRequest
// Mobile	The number to check
// CountryCode	2 Letter countrycode to validate number against
type FormatNumberRequest struct {
	BurstSMSRequest `json:"-" schema:"-"`
	Mobile          string `json:"msisdn" schema:"msisdn"`
	CountryCode     string `json:"countrycode" schema:"countrycode"`
}

func (r FormatNumberRequest) Validate() error {
	if err := r.BurstSMSRequest.Validate(); err != nil {
		return err
	}
	if !common.ValidateMobile(r.Mobile, r.CountryCode) {
		fmt.Printf("invalid mobile no %s %s", r.CountryCode, r.Mobile)
	}

	return common.ValidateCountryCode(r.CountryCode)
}

type GetSMSSentRequest struct {
	BurstSMSRequest `json:"-"                          schema:"-"`
	MessageID       uint   `json:"message_id"`
	OptOuts         string `json:"optouts"`
	*Page           `json:"page"`
	Max             uint   `json:"max"`
	Delivery        string `json:"delivery"`
}

func (r GetSMSSentRequest) Validate() error {
	if r.MessageID == 0 {
		fmt.Errorf("message_id is required")
	}
	if r.OptOuts != "" {
		if !(strings.ToLower(r.OptOuts) == "only" || strings.ToLower(r.OptOuts) == "omit" || strings.ToLower(r.OptOuts) == "include") {
			fmt.Errorf("optouts must be only, omit or include")
		}
	}
	if r.Delivery != "" {
		if !(strings.ToLower(r.Delivery) == "delivered" || strings.ToLower(r.Delivery) == "failed" || strings.ToLower(r.Delivery) == "pending") {
			fmt.Errorf("delivery must be delivered, failed or pending")
		}
	}
	return nil
}

func (r GetSMSSentRequest) Encode() (*strings.Reader, error) {
	form := url.Values{}
	enc := schema.NewEncoder()
	enc.RegisterEncoder(r.Delivery, func(v reflect.Value) string {
		if v.IsValid() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.MessageID, func(v reflect.Value) string {
		if v.Uint() > 0 {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.OptOuts, func(v reflect.Value) string {
		if v.IsValid() {
			return v.String()
		}
		return ""
	})
	enc.RegisterEncoder(r.Page, func(v reflect.Value) string {
		if v.IsNil() {
			return ""
		}
		return v.String()
	})
	err := enc.Encode(r, form)
	if err != nil {
		return nil, err
	}
	return strings.NewReader(form.Encode()), nil
}
