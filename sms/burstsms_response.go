package sms

type SendSMSResponse struct {
	MessageID  uint     `json:"message_id"`
	Recipients uint     `json:"recipients"`
	Cost       float64  `json:"cost"`
	SendAt     SendAt   `json:"send_at"`
	List       []NameID `json:"list"`
	Fails      []string `json:"fails"`
	*Error     `json:"error,omitempty"`
}

type NameID struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

type Number struct {
	CountryCode           uint   `json:"countrycode"`
	International         uint   `json:"international"`
	IsValid               bool   `json:"isValid"`
	NationalLeadingZeroes uint   `json:"national_leading_zeroes"`
	NationalNumber        uint   `json:"nationalnumber"`
	RawInput              string `json:"rawinput"`
	Type                  uint   `json:"type"`
}

type FormatNumberResponse struct {
	Number `json:"number,omitempty"`
	*Error `json:"error,omitempty"`
}

type Page struct {
	Count  uint
	Number uint
}

type Message struct {
	MessageID  uint `json:"message_id"`
	SendAt     `json:"sent_at"`
	Recipients uint `json:"recipients"`
	NameID     `json:"list"`
}

type Recipients struct {
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
	Mobile         string `json:"msisdn"`
	OptOut         bool   `json:"optout"`
	DeliveryStatus string `json:"delivery_status"`
}

type GetSMSSentResponse struct {
	Page       `json:"page"`
	Total      uint `json:"total"`
	Message    `json:"message"`
	Recipients []Recipients `json:"recipients"`
	*Error     `json:"error,omitempty"`
}
