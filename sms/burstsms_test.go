package sms

import (
	"fmt"
	"net/url"
	"testing"
	"time"
)

func TestSendSMS(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping send sms test in short mode for saving monies")
	}
	a := &BurstSMSAdapter{}
	apiURL, _ := url.Parse("https://api.transmitsms.com/send-sms.json")
	d, _ := time.ParseDuration("10s")
	sendAtTime := time.Now().UTC().Add(d)
	sendAt := &SendAt{&sendAtTime}
	req := &SendSMSRequest{
		BurstSMSRequest: BurstSMSRequest{
			ApiURL:    apiURL,
			ApiKey:    "ae559db613f7b722e21c7b1108037c46",
			ApiSecret: "secret",
		},
		To:          "61481829013",
		CountryCode: "AU",
		Message:     "Hi",
		SendAt:      sendAt,
	}
	resp, err := a.SendSMS(req)
	if err != nil {
		fmt.Printf("%v", err)
		t.FailNow()
		return
	}
	if resp.Error != nil && resp.Error.Code != "SUCCESS" {
		t.FailNow()
		return
	}
}

func TestFormatNumber(t *testing.T) {
	a := &BurstSMSAdapter{}
	apiURL, _ := url.Parse("https://api.transmitsms.com/format-number.json")
	req := &FormatNumberRequest{
		BurstSMSRequest: BurstSMSRequest{
			ApiURL:    apiURL,
			ApiKey:    "ae559db613f7b722e21c7b1108037c46",
			ApiSecret: "secret",
		},
		Mobile:      "61481829013",
		CountryCode: "AU",
	}
	if err := req.Validate(); err != nil {
		fmt.Printf("request invalid %v", err)
		t.FailNow()
	}
	resp, err := a.FormatNumber(req)
	if err != nil {
		fmt.Printf("%v", err)
		t.FailNow()
		return
	}
	if resp.Error != nil && resp.Error.Code != "SUCCESS" {
		t.FailNow()
		return
	}
}
