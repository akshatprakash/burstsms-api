package sms

import (
	"errors"
	"time"
)

const (
	ISO_8601_With_Time    = "2006-01-02 15:04:05"
	ISO_8601_Without_Time = "2006-01-02"
)

type Error struct {
	Code        string `json:"code"`
	Description string `json:"description"`
}

type SendAt struct {
	*time.Time
}

// MarshalJSON implements the json.Marshaler interface.
// The time is a quoted string in ISO 8601 format.
// TODO valid date time format (YYYY-MM-DD HH:MM:SS) or (YYYY-MM-DD)
func (t SendAt) MarshalJSON() ([]byte, error) {
	if y := t.Year(); y < 0 || y >= 10000 {
		// ISO 8601 is clear that years are 4 digits exactly.
		return nil, errors.New("Time.MarshalJSON: year outside of range [0,9999]")
	}
	format := ISO_8601_With_Time
	if t.Hour() == 0 && t.Minute() == 0 && t.Second() == 0 && t.Nanosecond() == 0 {
		format = ISO_8601_Without_Time
	}
	b := make([]byte, 0, len(format)+2)
	b = append(b, '"')
	b = t.AppendFormat(b, format)
	b = append(b, '"')
	return b, nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
// The time is expected to be a quoted string in ISO 8601 format.
// valid date time formats are (YYYY-MM-DD HH:MM:SS) or (YYYY-MM-DD)
func (t *SendAt) UnmarshalJSON(data []byte) error {
	// Ignore null, like in the main JSON package.
	if string(data) == "null" {
		return nil
	}
	// Fractional seconds are handled implicitly by Parse.
	format := ISO_8601_With_Time
	if len(data) == len(ISO_8601_Without_Time) {
		format = ISO_8601_Without_Time
	}
	tm, err := time.Parse(`"`+format+`"`, string(data))
	if err != nil {
		t.Time = &tm
	}
	return err
}

// String returns the time formatted using the format string
//	"YYYY-MM-DD HH:MM:SS" or "YYYY-MM-DD"
func (t SendAt) String() string {
	if t.Hour() == 0 && t.Minute() == 0 && t.Second() == 0 && t.Nanosecond() == 0 {
		return t.Format("2006-01-02")
	}
	return t.Format("2006-01-02 15:04:05")
}
